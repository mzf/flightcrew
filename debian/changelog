flightcrew (0.7.2+dfsg-14) unstable; urgency=high

  * New maintainer (Closes: #861997)
  * Fix CVE-2019-13032 (Closes: #931246)
  * Fix CVE-2019-13241
  * Add autopkg tests
  * Add Salsa continuous integration
  * Bump Standards-Version to 4.4.0

 -- Francois Mazen <francois@mzf.fr>  Sat, 06 Jul 2019 19:30:01 +0200

flightcrew (0.7.2+dfsg-13) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one.

  [ Mattia Rizzolo ]
  * Bump debhelper compat level to 12.
  * Bump Standards-Version to 4.3.0, no changes needed.

 -- Mattia Rizzolo <mattia@debian.org>  Tue, 08 Jan 2019 13:16:29 +0100

flightcrew (0.7.2+dfsg-12) unstable; urgency=medium

  [ Steven Robbins ]
  * Update patch to remove the requirement on libgtest-dev and libgmock-dev.

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 05 May 2018 10:31:56 +0200

flightcrew (0.7.2+dfsg-11) unstable; urgency=medium

  * Fix build with googletest >= 1.8.0-9 that moved header files around.
    Closes: #897152
  * d/control:
    + Build-Depend only on a subset of boost, instead of all of it.
    + Bump Standards-Version to 4.1.4, no changes needed.
    + Set Rules-Requires-Root:no.
    + Remove Don from Uploaders as he hasn't contributed in a while.
      Thank you for all your past work!

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 29 Apr 2018 15:51:54 +0200

flightcrew (0.7.2+dfsg-10) unstable; urgency=medium

  * d/control:
    + Move the packaging to salsa.d.o.
    + Move libflightcrew0v5 to Section:libs where it belongs.
  * Bump Standards-Version to 4.1.3:
    + Bump priority from extra (deprecated) to optional.
    + Use HTTPS in d/copyright's Format field.
  * d/copyright: bump copyright years for debian/*.
  * Bump debhelper compat level to 11.
  * d/rules: replace dh_install --fail-missing by dh_missing.

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 24 Feb 2018 18:09:34 +0100

flightcrew (0.7.2+dfsg-9) unstable; urgency=medium

  * d/copyright: claim copyright for the 2017.
  * Add patch to fix a security issue due to insecure use of /tmp.
    Thanks to Jakub Wilk <jwilk@jwilk.net> for the report and to
    Thomas Pierson <contact@thomaspierson.fr> for the patch.
    Closes: #861987

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 15 May 2017 13:04:05 +0200

flightcrew (0.7.2+dfsg-8) unstable; urgency=medium

  * Bump debhelper compat level to 10.
    + --parallel is now default.
  * Add missing Build-Depends on libgtest-dev.
    Thanks to Gianfranco Costamagna for the patch.  Closes: #844856

 -- Mattia Rizzolo <mattia@debian.org>  Thu, 24 Nov 2016 12:36:34 +0100

flightcrew (0.7.2+dfsg-7) unstable; urgency=medium

  * debian/control:
    + Bump Standards-Version to 3.9.8, no changes needed.
    + Use HTTPS in Vcs-Git.
    + wrap-and-sort -ast
    + Drop non-existant misc:Recommends and shlibs:Recommends substvars.

 -- Mattia Rizzolo <mattia@debian.org>  Thu, 26 May 2016 18:12:24 +0000

flightcrew (0.7.2+dfsg-6) unstable; urgency=medium

  * [7ccf0ab] debian/patches/reproducible-build: also sort the sources of
    xerces before compiling/linking them.

 -- Mattia Rizzolo <mattia@debian.org>  Tue, 08 Dec 2015 09:35:22 +0000

flightcrew (0.7.2+dfsg-5) unstable; urgency=medium

  * [fb787bf] debian/patches/reproducible-build: also sort the sources of
    zipios before compiling/linking them.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 07 Dec 2015 18:06:12 +0000

flightcrew (0.7.2+dfsg-4) unstable; urgency=medium

  * [2f174b8] debian/patches/reproducible-build: also sort the sources of
    flightcrew-gui before compiling/linking them.

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 06 Dec 2015 19:42:33 +0000

flightcrew (0.7.2+dfsg-3) unstable; urgency=medium

  * [fba6efa] debian/{control,copyright}: use my @debian.org email address.
  * [c33d445] debian/patch/reproducible-build: add to make the build
    reproducible independent from readdir() order.

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 06 Dec 2015 00:05:50 +0000

flightcrew (0.7.2+dfsg-2) unstable; urgency=medium

  * [56f6b9c] debian/rules: enable parallel building
  * [89b9213] libflightcrew0 → libflightcrew0v5 for the libstdc++6 transition

 -- Mattia Rizzolo <mattia@mapreri.org>  Sat, 29 Aug 2015 17:15:30 +0000

flightcrew (0.7.2+dfsg-1) experimental; urgency=low

  [ Don Armstrong ]
  * Initial packaging (closes: #602781)

 -- Mattia Rizzolo <mattia@mapreri.org>  Thu, 12 Feb 2015 18:11:30 +0100
