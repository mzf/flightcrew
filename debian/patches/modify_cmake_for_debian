Description: Modify CmakeLists for Debian
 Avoid using the boundled libraries but link the system ones.
Author: Don Armstrong <don@debian.org>
Author: Mattia Rizzolo <mattia@mapreri.org>
Author: Francois Mazen <francois@mzf.fr>
Forwarded: no
Last-Update: 2018-05-02

Index: flightcrew/CMakeLists.txt
===================================================================
--- flightcrew.orig/CMakeLists.txt
+++ flightcrew/CMakeLists.txt
@@ -41,12 +41,8 @@ set( CMAKE_OSX_SYSROOT "/Developer/SDKs/
 set( CMAKE_OSX_ARCHITECTURES "i386;x86_64" )
 
 # The parsing order is significant!
-add_subdirectory( src/BoostParts )
-add_subdirectory( src/Xerces )
 add_subdirectory( src/XercesExtensions )
-add_subdirectory( src/zlib )
 add_subdirectory( src/zipios )
-add_subdirectory( src/googlemock )
 add_subdirectory( src/FlightCrew )
 add_subdirectory( src/FlightCrew-cli )
 
Index: flightcrew/src/FlightCrew-cli/CMakeLists.txt
===================================================================
--- flightcrew.orig/src/FlightCrew-cli/CMakeLists.txt
+++ flightcrew/src/FlightCrew-cli/CMakeLists.txt
@@ -59,6 +59,7 @@ if( MSVC )
 # "Print all warnings" flag for GCC
 elseif( CMAKE_COMPILER_IS_GNUCXX )
     add_definitions( -Wall )
+    set_target_properties( ${PROJECT_NAME} PROPERTIES LINK_FLAGS -Wl,--as-needed )
 endif()
 
 set_source_files_properties( main.cpp PROPERTIES COMPILE_DEFINITIONS FLIGHTCREW_FULL_VERSION="${FLIGHTCREW_FULL_VERSION}" )
Index: flightcrew/src/FlightCrew/CMakeLists.txt
===================================================================
--- flightcrew.orig/src/FlightCrew/CMakeLists.txt
+++ flightcrew/src/FlightCrew/CMakeLists.txt
@@ -50,9 +50,7 @@ list( REMOVE_ITEM SOURCES ${to_remove} )
 
 # creating PCH's for MSVC and GCC on Linux
 include( ${CMAKE_SOURCE_DIR}/cmake_extras/CustomPCH.cmake )
-set( ALL_INCLUDES ${BoostParts_SOURCE_DIR}
-                  ${Xerces_SOURCE_DIR}
-                  ${zipios_SOURCE_DIR} )
+set( ALL_INCLUDES ${zipios_SOURCE_DIR} )
 set( GCC_PCH_TARGET gccPCH_fc )
 
 precompiled_header( SOURCES ALL_INCLUDES ${GCC_PCH_TARGET} ${PCH_NAME} )
@@ -65,8 +63,6 @@ precompiled_header( SOURCES ALL_INCLUDES
 include_directories( ${CMAKE_CURRENT_BINARY_DIR}
                      ${CMAKE_CURRENT_SOURCE_DIR}
                      ../zipios 
-                     ../BoostParts 
-                     ../Xerces
                      ../XercesExtensions
                      ../utf8-cpp
                    )
@@ -78,11 +74,29 @@ if( BUILD_SHARED_FC )
     # when they want a dll, but NOT FC_DLL_EXPORTING
     add_definitions( -DFC_DLL_EXPORTING -DFC_BUILT_AS_DLL )
     add_library( ${PROJECT_NAME} SHARED ${SOURCES} )
+    SET_TARGET_PROPERTIES(
+      ${PROJECT_NAME}
+      PROPERTIES
+      SOVERSION 0
+      VERSION 0.7.2
+      )
 else()
     add_library( ${PROJECT_NAME} ${SOURCES} )
 endif()
 
-target_link_libraries( ${PROJECT_NAME} zipios BoostParts XercesExtensions )
+find_library(LIB_ZLIB z)
+find_library(BOOST_DT boost_date_time)
+find_library(BOOST_FS boost_filesystem)
+find_library(BOOST_PO boost_program_options)
+find_library(BOOST_RE boost_regex)
+find_library(BOOST_S boost_system)
+find_library(BOOST_T boost_thread)
+find_library(LIB_XERCES xerces-c)
+find_library(PTHREAD pthread)
+SET_TARGET_PROPERTIES(${PROJECT_NAME} PROPERTIES LINK_FLAGS -Wl,--as-needed)
+target_link_libraries( ${PROJECT_NAME} ${LIB_ZLIB} ${BOOST_DT} ${BOOST_FS} ${BOOST_PO} ${BOOST_RE} ${BOOST_S} ${BOOST_T} zipios ${LIB_XERCES} XercesExtensions ${PTHREAD} )
+INSTALL(TARGETS ${PROJECT_NAME} LIBRARY DESTINATION lib COMPONENT libraries)
+INSTALL(FILES flightcrew.h exception.h DllExporting.h Result.h Misc/ErrorResultCollector.h ResultId.h ResultType.h ../XercesExtensions/NodeLocationInfo.h DESTINATION include/${PROJECT_NAME})
 
 #############################################################################
 
@@ -115,9 +129,6 @@ elseif( CMAKE_COMPILER_IS_GNUCXX )
     add_dependencies( ${PROJECT_NAME} ${GCC_PCH_TARGET} )
 endif()
 
-# needed for correct Xerces header inclusion
-add_definitions( -DXERCES_STATIC_LIBRARY )
-
 #############################################################################
 
 # We don't build the tests when fc is built as a shared
Index: flightcrew/src/FlightCrew-gui/CMakeLists.txt
===================================================================
--- flightcrew.orig/src/FlightCrew-gui/CMakeLists.txt
+++ flightcrew/src/FlightCrew-gui/CMakeLists.txt
@@ -67,7 +67,7 @@ create_source_groups( RAW_SOURCES )
 # and the headers for the linked-to libraries
 include_directories( ${CMAKE_CURRENT_SOURCE_DIR}
                      ${CMAKE_CURRENT_BINARY_DIR}
-                     ${BoostParts_SOURCE_DIR}
+#                     ${BoostParts_SOURCE_DIR}
                      ${FlightCrew_SOURCE_DIR}
                      ${XercesExtensions_SOURCE_DIR}
                      )
@@ -110,6 +110,7 @@ if( MSVC )
 # "Print all warnings" flag for GCC
 elseif( CMAKE_COMPILER_IS_GNUCXX )
     add_definitions( -Wall )
+    set_target_properties( ${PROJECT_NAME} PROPERTIES LINK_FLAGS -Wl,--as-needed )
 endif()
 
 set_source_files_properties( MainWindow.cpp PROPERTIES COMPILE_DEFINITIONS FLIGHTCREW_FULL_VERSION="${FLIGHTCREW_FULL_VERSION}" )
@@ -135,6 +136,10 @@ if( APPLE )
                         WORKING_DIRECTORY ${WORK_DIR}
                         DEPENDS addframeworks )
                 
+elseif(DEBIAN)
+        # these rules are so that we don't do any crazy installer
+        #  building which we've stripped out anyway
+        set( WORK_DIR "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}" )
 # For Linux and Windows, provide binary installers.
 # For this to work on Linux, InstallJammer needs to be installed and on the system path.
 # For this to work on Windows, Inno Setup's iscc compiler needs to be installed and on the system path.
Index: flightcrew/src/FlightCrew/tests/CMakeLists.txt
===================================================================
--- flightcrew.orig/src/FlightCrew/tests/CMakeLists.txt
+++ flightcrew/src/FlightCrew/tests/CMakeLists.txt
@@ -41,12 +41,12 @@ list( REMOVE_ITEM TEST_SOURCES ${to_remo
 # That means that subdirs inherit the include_directories of their parents.
 # So techincally we don't need to include Xerces etc.
 include_directories( ${CMAKE_CURRENT_BINARY_DIR} 
-                     ${CMAKE_CURRENT_SOURCE_DIR} 
-                     ../../BoostParts 
-                     ../../Xerces 
                      ../../XercesExtensions 
-                     ../../googlemock/include
-                     ../../googlemock/gtest/include
+                     /usr/src/googletest/googlemock/include
+                     /usr/src/googletest/googlemock
+                     /usr/src/googletest/googletest/include
+                     /usr/src/googletest/googletest
+                     ${CMAKE_CURRENT_SOURCE_DIR}
                    )
 
 link_directories ( ${PROJECT_BINARY_DIR}/lib ) 
@@ -55,7 +55,7 @@ link_directories ( ${PROJECT_BINARY_DIR}
 
 # creating PCH's for MSVC and GCC on Linux
 include( ${CMAKE_SOURCE_DIR}/cmake_extras/CustomPCH.cmake )
-set( ALL_INCLUDES ${gtest_SOURCE_DIR}/include ${BoostParts_SOURCE_DIR} )
+set( ALL_INCLUDES /usr/src/googletest/googletest/include ${BoostParts_SOURCE_DIR} )
 
 set( GCC_PCH_TARGET gccPCH_tests )
 
@@ -65,7 +65,12 @@ precompiled_header( TEST_SOURCES ALL_INC
 
 add_executable( ${PROJECT_NAME} ${TEST_SOURCES} )
 
-target_link_libraries( ${PROJECT_NAME} FlightCrew gmock )
+add_library( GOOGLE_MOCK_LIB STATIC
+    /usr/src/googletest/googlemock/src/gmock-all.cc
+    /usr/src/googletest/googletest/src/gtest-all.cc
+)
+
+target_link_libraries( ${PROJECT_NAME} FlightCrew GOOGLE_MOCK_LIB )
 
 #############################################################################
 
@@ -101,7 +106,7 @@ elseif( CMAKE_COMPILER_IS_GNUCXX )
 endif()
 
 # needed for correct Xerces header inclusion
-add_definitions( -DXERCES_STATIC_LIBRARY )
+# add_definitions( -DXERCES_STATIC_LIBRARY )
 
 #############################################################################
 
Index: flightcrew/src/XercesExtensions/CMakeLists.txt
===================================================================
--- flightcrew.orig/src/XercesExtensions/CMakeLists.txt
+++ flightcrew/src/XercesExtensions/CMakeLists.txt
@@ -16,14 +16,16 @@ file( GLOB SOURCES *.cpp *.h )
 # We need to pick up the stdafx.h file
 # and the headers for the linked-to libraries
 include_directories( ${CMAKE_CURRENT_SOURCE_DIR}
-                     ../Xerces
-                     ../BoostParts )
+#                     ../Xerces
+#                     ../BoostParts 
+)
 
 link_directories ( ${PROJECT_BINARY_DIR}/lib ) 
 
 add_library( ${PROJECT_NAME} ${SOURCES} )
 
-target_link_libraries( ${PROJECT_NAME} Xerces )
+find_library(LIB_XERCES xerces-c)
+target_link_libraries( ${PROJECT_NAME} ${LIB_XERCES} )
 
 #############################################################################
 
@@ -45,9 +47,10 @@ if( MSVC )
 
 # "Print all warnings" flag for GCC
 elseif( CMAKE_COMPILER_IS_GNUCXX )
+  SET_TARGET_PROPERTIES( ${PROJECT_NAME} PROPERTIES COMPILE_FLAGS -fPIC)
     add_definitions( -Wall )
 endif()
 
 # needed for correct Xerces header inclusion
-add_definitions( -DXERCES_STATIC_LIBRARY )
+# add_definitions( -DXERCES_STATIC_LIBRARY )
 
Index: flightcrew/src/zipios/CMakeLists.txt
===================================================================
--- flightcrew.orig/src/zipios/CMakeLists.txt
+++ flightcrew/src/zipios/CMakeLists.txt
@@ -25,7 +25,15 @@ link_directories ( ${PROJECT_BINARY_DIR}
 
 add_library( ${PROJECT_NAME} ${SOURCES} )
 
-target_link_libraries( ${PROJECT_NAME} zlib BoostParts )
+
+find_library(LIB_ZLIB z)
+find_library(BOOST_DT boost_date_time)
+find_library(BOOST_FS boost_filesystem)
+find_library(BOOST_PO boost_program_options)
+find_library(BOOST_RE boost_regex)
+find_library(BOOST_S boost_system)
+find_library(BOOST_T boost_thread)
+target_link_libraries( ${PROJECT_NAME} ${LIB_ZLIB} ${BOOST_DT} ${BOOST_FS} ${BOOST_PO} ${BOOST_RE} ${BOOST_S} ${BOOST_T})
 
 #############################################################################
 
@@ -44,6 +52,7 @@ if( MSVC )
 
 # "Print all warnings" flag for GCC
 elseif( CMAKE_COMPILER_IS_GNUCXX )
+  SET_TARGET_PROPERTIES( ${PROJECT_NAME} PROPERTIES COMPILE_FLAGS -fPIC)
     add_definitions( -Wall )
 endif()
 
